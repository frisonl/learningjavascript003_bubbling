A simple experiment to understand bubbling.

Based on https://www.youtube.com/watch?v=E-LBbqgM6OU 

It explores the difference between preventDefault and stopPropagation.